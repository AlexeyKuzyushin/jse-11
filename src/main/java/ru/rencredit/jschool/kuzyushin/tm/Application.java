package ru.rencredit.jschool.kuzyushin.tm;

import ru.rencredit.jschool.kuzyushin.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}
