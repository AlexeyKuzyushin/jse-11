package ru.rencredit.jschool.kuzyushin.tm.api.controller;

public interface ICommandController {

    void exit();

    void showVersion();

    void showAbout();

    void showHelp();

    void showInfo();

    void showCommands();

    void showArguments();
}