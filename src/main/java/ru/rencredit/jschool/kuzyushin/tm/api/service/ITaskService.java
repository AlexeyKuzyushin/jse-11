package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void add(Task task);

    void remove(Task task);

    List<Task> findALl();

    void clear();

    void create(String name);

    void create(String name, String description);
}
