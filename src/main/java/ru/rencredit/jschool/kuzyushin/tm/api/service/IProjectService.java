package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void add(Project project);

    void remove(Project project);

    List<Project> findALl();

    void clear();

    void create(String name);

    void create(String name, String description);
}
