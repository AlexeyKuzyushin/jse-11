package ru.rencredit.jschool.kuzyushin.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();
}
