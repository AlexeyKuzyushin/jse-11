package ru.rencredit.jschool.kuzyushin.tm.repository;

import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.constant.ArgumentConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.CommandConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.DescriptionConst;
import ru.rencredit.jschool.kuzyushin.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP, DescriptionConst.HELP
    );

    public static final Command ARGUMENTS = new Command(
            CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, DescriptionConst.ARGUMENTS
    );

    public static final Command COMMANDS = new Command(
            CommandConst.COMMANDS, ArgumentConst.COMMANDS, DescriptionConst.COMMANDS
    );

    public static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT, DescriptionConst.ABOUT
    );

    public static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION, DescriptionConst.VERSION
    );

    public static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO, DescriptionConst.INFO
    );

    public static final Command EXIT = new Command(
            CommandConst.EXIT, null, DescriptionConst.EXIT
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null, DescriptionConst.TASK_CREATE
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null, DescriptionConst.TASK_CLEAR
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null, DescriptionConst.TASK_LIST
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null, DescriptionConst.PROJECT_CREATE
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null, DescriptionConst.PROJECT_CLEAR
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null, DescriptionConst.PROJECT_LIST
    );

    private final String[] ARRAY_ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    private final String[] ARRAY_COMMANDS = getCommands(TERMINAL_COMMANDS);

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, EXIT, TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST
    };

    public String[] getArguments(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArgument();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    @Override
    public String[] getCommands() {
        return ARRAY_COMMANDS;
    }

    @Override
    public String[] getArguments() {
        return ARRAY_ARGUMENTS;
    }

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getCommand();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }
}
