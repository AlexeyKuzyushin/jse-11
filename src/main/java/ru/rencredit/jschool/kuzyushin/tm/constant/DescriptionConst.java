package ru.rencredit.jschool.kuzyushin.tm.constant;

public interface DescriptionConst {

    String HELP = " - Display terminal commands.";

    String VERSION = "Show version info.";

    String ABOUT = "Show developer info.";

    String INFO = "Show system info";

    String EXIT = "Close application";

    String ARGUMENTS = "Show program arguments";

    String COMMANDS = "Show program commands";

    String TASK_LIST = "Show task list";

    String TASK_CLEAR = "Remove all tasks";

    String TASK_CREATE = "Create new task";

    String PROJECT_LIST = "Show project list";

    String PROJECT_CLEAR = "Remove all projects";

    String PROJECT_CREATE = "Create new project";
}
