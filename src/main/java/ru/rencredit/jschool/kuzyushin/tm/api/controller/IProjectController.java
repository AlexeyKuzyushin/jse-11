package ru.rencredit.jschool.kuzyushin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();
}
